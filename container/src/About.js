import React from 'react';
import './about.css';

const About = () => (
  <main id="about">
    <h2>About this site</h2>
    <p>This is a demo site used to demonstrate one implemention of micro frontend architecture.</p>
  </main>
);

export default About;
