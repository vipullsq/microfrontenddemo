import React, {Suspense} from 'react';
import RestaurantDetails from './RestaurantDetails';

const ShowPrice = React.lazy(() => import('./ShowPrice'));

class Restaurant extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      error: false,
      restaurant: null,
      showPrice: false,
      price: null,
    };
  }

  generateRandomPrice() {
    return Math.floor(Math.random() * 100);
  }

  toggleShowPrice = () => {
    this.setState(state => ({
      showPrice: !state.showPrice,
      price: this.generateRandomPrice()
    }));
  }

  componentDidMount() {
    const host = process.env.REACT_APP_CONTENT_HOST;
    const id = this.props.match.params.id;

    fetch(`${host}/restaurants.json`)
      .then(result => result.json())
      .then(restaurants => {
        let restaurant = restaurants[id-1];
        this.setState({
          restaurant: {
            ...restaurant,
            imageSrc: `${host}${restaurant.imageSrc}`,
          },
          loading: false,
        });
      })
      .catch(() => {
        this.setState({ loading: false, error: true });
      });
  }

  render() {
    if (this.state.loading) {
      return 'Loading';
    }
    if (this.state.error) {
      return 'Sorry, but that restaurant is currently unavailable.';
    }

    return ( 
    <Suspense fallback={<div>Loading...</div>}>
      <RestaurantDetails restaurant={this.state.restaurant} />
      <div style={{
        width: '300px',
        margin: '20px auto',
        fontSize: '24px',
        textAlign: 'center',
      }}>
        { this.state.showPrice && <ShowPrice price={this.state.price} /> }
        <button onClick={this.toggleShowPrice}>{this.state.showPrice ? 'Hide Price' : 'Show Price'}</button>
      </div>
    </Suspense>
  )}
}

export default Restaurant;
