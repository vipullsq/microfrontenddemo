import React from 'react';
import styled from 'styled-components';

const smallScreen = `@media(max-width: 1080px)`;

const Container = styled.div`
  display: flex;
  justify-content: space-around;

  ${smallScreen} {
    flex-direction: column;
    align-items: center;
  }
`;


const ImageColumn = styled.section`
  max-width: 100%;
  ${smallScreen} {
    order: -1;
  }
`;

const RestaurantName = styled.h1`
  font-size: 40px;
  margin: 10px 0 20px;
`;

const Img = styled.img`
  width: 500px;
  max-width: 100%;
  margin-bottom: 10px;
`;

const Figure = styled.figure`
  margin: 0;
`;

const Caption = styled.figcaption`
  width: 500px;
  max-width: 100%;
  font-size: 30px;
`;


class RestaurantDetails extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      name,
      imageSrc,
      imageDescription,
      description,
    } = this.props.restaurant;


    return (
      <Container>
        <ImageColumn>
          <RestaurantName>{name}</RestaurantName>
          <Figure>
            <Img src={imageSrc} alt={imageDescription} />
            <Caption>{description}</Caption>
          </Figure>
        </ImageColumn>
      </Container>
    );
  }
}
export default RestaurantDetails;
