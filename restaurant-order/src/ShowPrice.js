import React from 'react';

function ShowPrice({price}) {
    return <p>$ {price}</p>
}

export default ShowPrice;
