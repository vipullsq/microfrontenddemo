import React from 'react';

function Test() {
    return (
        <p>This is a test component to be loaded lazily</p>
    )
}

export default Test;